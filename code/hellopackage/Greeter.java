package hellopackage;
import java.util.*;
import secondpackage.Utilities;


public class Greeter {
    public static void main(String[] args){
    Scanner reader=new Scanner(System.in);

    Random rad=new Random();

    System.out.println("Enter an integer");
    int num= reader.nextInt();

    int solution=Utilities.doubleMe(num);

    System.out.println("Your value is: " + solution);
    }
    
}